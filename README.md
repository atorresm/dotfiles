Dotfiles for:  
* vim
* i3wm
* i3blocks
* dunst
* zsh
* oh-my-zsh
* dmenu
* rofi
* xfce4-terminal
* xscreensaver
* xbindkeys

[Telegram theme](https://github.com/gilbertw1/telegram-gruvbox-theme)  
Using [NerdFonts](https://github.com/ryanoasis/nerd-fonts) (Patched Roboto (not-mono) in `fonts`).
