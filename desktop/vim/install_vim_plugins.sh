#!/bin/bash
create_install_dirs() {
	echo "Creating ~/.vim/autoload dir..."
	mkdir -p ~/.vim/autoload
	echo "Creating ~/.vim/bundle dir..."
	mkdir -p ~/.vim/bundle
}

install_pathogen() {
	echo -e "\nInstalling Pathogen..."
	curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
	
}

download_plugins() {
    cd ~/.vim/bundle

	echo -e "\nInstalling NERDtree..."
	git clone https://github.com/scrooloose/nerdtree

    echo -e "\nInstalling NERD Commenter..."
    git clone https://github.com/scrooloose/nerdcommenter
    
    echo -e "\nInstalling Syntastic..."
    git clone https://github.com/scrooloose/syntastic

	echo -e "\nInstalling Airline..."
	git clone https://github.com/bling/vim-airline

	echo -e "\nInstalling clang-format..."
	git clone https://github.com/rhysd/vim-clang-format

	echo -e "\nInstalling colorschemes..."
	git clone https://github.com/flazz/vim-colorschemes

	echo -e "\nInstalling cpp-enhanced-highlight..."
	git clone https://github.com/octol/vim-cpp-enhanced-highlight

	echo -e "\nInstalling rainbow parentheses..."
	git clone https://github.com/kien/rainbow_parentheses.vim

	echo -e "\nInstalling gruvbox colorscheme..."
	git clone https://github.com/morhetz/gruvbox    
}

echo "The plugins will be downloaded to ~/.vim/bundle, to use with Pathogen. Press Enter."
read  -n 1 mainmenuinput
if ["$mainmenuinput" = ""]; then
	create_install_dirs
	install_pathogen
	download_plugins
	echo -e "The plugins were installed succesfully!"
fi


