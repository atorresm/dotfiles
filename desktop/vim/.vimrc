" Vim config
set nocompatible
filetype plugin on	"filetype plugin
syntax enable       "syntax highlighting		
" enable pathogen:
execute pathogen#infect() 

" Basic config
let mapleader=','
set showcmd		"show command
set number		"show line no.
set ruler		"show row and column
set mouse=a		"allow copypasting and mouse
set encoding=utf-8	"encoding
set cul  "highlight current line
set listchars=eol:¬,tab:\|→,extends:>,precedes:<,trail:·,nbsp:␣
set laststatus=2

" Convert :W to :w and :Q to :q
fun! SetupCommandAlias(from, to)
  exec 'cnoreabbrev <expr> '.a:from
        \ .' ((getcmdtype() is# ":" && getcmdline() is# "'.a:from.'")'
        \ .'? ("'.a:to.'") : ("'.a:from.'"))'
endfun
call SetupCommandAlias("W","w")
call SetupCommandAlias("Q","q")

" Colors
set t_Co=256		"terminal with 256 colors
set termguicolors
"colorscheme srcery-drk 
colorscheme gruvbox
set background=dark

" Tabs
set shiftwidth=4
set tabstop=4
set softtabstop=4
set shiftround
set expandtab
set smarttab

" Scroll
set scrolloff=3         " scroll when cursor gets within 3 characters of top/bottom edge
set sidescrolloff=5     " scroll when cursor gets within 5 characters of left/right edge
set sidescroll=20       " scroll by 20 characters when scrolling horizontally
set colorcolumn=80      " highlight the 80 column

" Indentation
set autoindent
set smartindent
filetype indent on

" Comments
set formatoptions+=o
set formatoptions+=r

" Buffer settings
set hidden
" open new empty buffer
nmap <leader>T :enew<cr>     
" move to next buffer
nmap <leader>l :bnext<CR>
" move to the previous buffer
nmap <leader>h :bprevious<CR>
" Close the current buffer and move to the previous one
" This replicates the idea of closing a tab
nmap <leader>bq :bp <BAR> bd #<CR>
" Show all open buffers and their status
nmap <leader>bl :ls<CR>

" Installed plugins:
" nerdtree
" nerd commenter
" syntastic
" vim-airline (w/ tabline)
" vim-clang-format
" vim-colorschemes
" vim-cpp-enhanced-highlight
" rainbom_parentheses
" colorscheme: gruvbox (dark)
" font: fira mono medium 12 (powerline fix) 

" rainbow_parentheses
au VimEnter * RainbowParenthesesToggle

" NERDTree
map <F8> :NERDTreeToggle<CR>    "F8 to toggle nerdtree
let NERDTreeWinPos = "left"
" quit nerdtree if it's the last buffer
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" C++ Enhanced Higlighting
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:cpp_experimental_template_highlight = 1
let g:cpp_concepts_highlight = 1

" ClangFormat
map <leader>f :ClangFormat<CR>

" Syntastic settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_cpp_check_header = 1
let g:syntastic_c_compiler = 'gcc'
let g:syntastic_cpp_compiler = 'g++'
let g:syntastic_cpp_compiler_options = '-std=c++11'

" vim-airline settings
let g:airline_powerline_fonts = 1
let g:airline#extensions#syntastic#enabled = 1
let g:airline#extensions#tabline#enabled = 1
"let g:airline_theme='dark'
let g:airline_theme='gruvbox'
